class YAINA {
	tag = "YAINA";

	class Main {
		file = "Functions\YAINA\Main";
		class baseCleanupManager { postInit = 1; };
		class timeManager{ postInit=1; };
		class YhideTerrainObjects{};
		class showTerrainObjects{};
		class getPointBetween {};
		class postInit { postInit = 1; };
		class markerManager { postInit = 1; };
		class preInit { preInit = 1; };
		class dirFromNearestName {};
		class addRewardPoints {};
		class globalHint {};
		class getPosAround {};
		class log {};
		class addActionMP {};
		class killLog {};
		class deleteVehicleIn {};
		class kickSelf {};
		class playerIntroComplete {};
		class getDBKey {};
        class getFunctions {};
    };
};